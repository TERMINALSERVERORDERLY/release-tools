## Preparation

- Preparation MR's should already be created
- [ ] Perform automated merging into the preparation branches:
    ```sh
    # In Slack
    /chatops run release merge <%= version.to_ce %>
    ```

- [ ] Ensure the CE preparation MR has been fully merged into the EE counterpart
- [ ] Merge the preparation branches
- [ ] For `omnibus-gitlab` add the changes to the stable branches:
  - Before the 7th: merge master into the CE stable branch, then merge the CE stable branch into EE.
  - After the 7th: cherry-pick [remaining merge requests] directly into CE stable branch. Then, merge the CE Omnibus stable branch into EE.
- Check the following list of critical issues/MRs which are to be included in `<%= version %>`. Ensure each has made both CE and EE:
  - [ ] REFERENCE_TO_MR_TO_PICK
- [ ] Ensure builds are green on [Omnibus CE stable branch] and [Omnibus EE stable branch]

[preparation MRs]: https://gitlab.com/gitlab-org/release/docs/blob/master/general/picking-into-merge-requests.md
[remaining merge requests]: https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Pick%20into%20<%= version.to_minor %>
[Omnibus CE stable branch]: https://gitlab.com/gitlab-org/omnibus-gitlab/commits/<%= version.stable_branch %>
[Omnibus EE stable branch]: https://gitlab.com/gitlab-org/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>

## Packaging

- [ ] [Sync stable branches](https://gitlab.com/gitlab-org/release/docs/blob/master/general/push-to-multiple-remotes.md) to `dev`
    - [ ] [CE](https://dev.gitlab.org/gitlab/gitlabhq/commits/<%= version.stable_branch %>)
    - [ ] [EE](https://dev.gitlab.org/gitlab/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>)
    - [ ] [Omnibus](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>)
- [ ] Tag `<%= version %>`:

  ```sh
  # In Slack:
  /chatops run release tag <%= version %>
  ```
- [ ] While waiting for packages to build, now is a good time to [prepare the blog post]. Look at previous MRs for examples. => BLOG_POST_MR
- [ ] Check progress of [EE packages build](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: true) %>) and [CE packages build](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: false) %>).
    - This might take a while (around 80 min).
    - We only need the EE packages to finish to continue with next steps.


[prepare the blog post]: https://about.gitlab.com/handbook/marketing/blog/release-posts/#release-posts

## Deploy

Deploys to production require confirmation from a production team member
before proceeding. Use `/chatops run oncall production` in the
`#production` channel to find who's on call and ping someone. Deploys to
staging or canary can be done at will, just mention it in the
`#production` channel.

### staging.gitlab.com

- [ ] Inform the oncall in the #production channel about **staging** deploy
- [ ] Check if there are any [post-deployment
      patches](https://dev.gitlab.org/gitlab/post-deployment-patches/tree/master/<%=
      version.to_minor %>) that need to be re-applied. If there are,
      the deployment must be halted and assessed as to the impact of undoing the
      patches because of the release. To proceed, approval must be given by the
      manager oncall.
- [ ] [Deploy] `<%= version %>` to **staging.gitlab.com**. Note that starting in release **11.6** staging deploys are automatically triggered from the  [EE omnibus pipeline](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus(ee: true) %>) `gitlab_com:upload_deploy` stage.
    ```sh
    # In Slack:
    /chatops run deploy <%= version %>-ee.0
    ```
- [ ] Link to deployment job (even failed attempts) =>

#### QA

- [ ] Create a "QA Task" issue using the ChatOps command:
  ```sh
  # In Slack, replacing LAST_DEPLOYED_VERSION with the appropriate value:
  /chatops run release qa vLAST_DEPLOYED_VERSION v<%= version %>
  ```

### gitlab.com canary stage


- [ ] Inform the oncall in the #production channel about **canary** deploy
- [ ] [Deploy] `<%= version %>` to canary vms in production

    ```sh
    # In Slack:
    /chatops run deploy <%= version %>-ee.0 --production --canary
    ```
- [ ] Link to deployment job (even failed attempts) =>
- [ ] Confirm that there are no errors on canary
  - [canary errors on sentry.gitlab.net](https://sentry.gitlab.net/gitlab/gitlabcom/?query=server_name%3A%22web-cny-01-sv-gprd%22)
  - [canary dashboard](https://dashboards.gitlab.net/d/llfd4b2ik/canary)

**If there are issues on canary you should immediately stop sending traffic to it by issuing the following chatops command**:

```
/chatops run canary --drain --production

```

### gitlab.com main stage (production)

- [ ] Wait for the QA Task deadline to pass before deploying to the rest of gitlab.com
- [ ] Confirm there are no critical alerts on gitlab.com on the [alerting dashboard](https://dashboards.gitlab.net/d/SOn6MeNmk/alerts)
- [ ] Get confirmation from a production team member to deploy to **production**
      In `#production`, use `/chatops run oncall production` to find who's on call, and `@mention` them asking to deploy to production
- [ ] If someone besides the oncall confirms, `@mention` the oncall so they are aware.
- [ ] [Deploy] `<%= version %>` to GitLab.com

    ```sh
    # In Slack:
    /chatops run deploy <%= version %>-ee.0 --production
    ```
- [ ] Link to deployment job (even failed attempts) =>
- [ ] Check if there are any [post-deployment
      patches](https://dev.gitlab.org/gitlab/post-deployment-patches/tree/master/<%=
      version.to_minor %>) that need to be re-applied. If there are,
      the deployment must be halted and assessed as to the impact of undoing the
      patches because of the release. To proceed, approval must be given by the
      manager oncall.

[Deploy]: https://gitlab.com/gitlab-org/takeoff#deploying-gitlab
[announce the deploy]: https://gitlab.com/gitlab-org/takeoff/blob/master/doc/announce-a-deployment.md

## Release

- [ ] Publish the packages via ChatOps:
  ```
  # In Slack:
  /chatops run publish <%= version %>
  ```
- [ ] Verify that packages appear on `packages.gitlab.com`: [EE](https://packages.gitlab.com/gitlab/gitlab-ee) / [CE](https://packages.gitlab.com/gitlab/gitlab-ce)
- [ ] Verify that Docker images appear on `hub.docker.com`: [EE](https://hub.docker.com/r/gitlab/gitlab-ee/tags) / [CE](https://hub.docker.com/r/gitlab/gitlab-ce/tags)
- [ ] Create the `<%= version %>` version on [version.gitlab.com](https://version.gitlab.com/versions/new?version=<%= version %>)
- [ ] Deploy the blog post
- [ ] Post a [tweet about] the `<%= version %>` release in the `#releases` channel:

    ```
    !tweet "GitLab <%= version %> is now available: [BLOG_POST_URL] [DESCRIPTION_OF_CHANGES]"
    ```

[tweet about]: https://gitlab.com/gitlab-org/takeoff#announce-that-the-deploy-is-done

## References

### gitlab.com

- https://gitlab.com/gitlab-org/gitlab-ce/commits/<%= version.stable_branch %>
- https://gitlab.com/gitlab-org/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>
- https://gitlab.com/gitlab-org/omnibus-gitlab/commits/<%= version.stable_branch %>
- https://gitlab.com/gitlab-org/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>

### dev.gitlab.org

- https://dev.gitlab.org/gitlab/gitlabhq/commits/<%= version.stable_branch %>
- https://dev.gitlab.org/gitlab/gitlab-ee/commits/<%= version.stable_branch(ee: true) %>
- https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch %>
- https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.stable_branch(ee: true) %>

/milestone %"<%= version.to_minor %>"
/due in 7 days
